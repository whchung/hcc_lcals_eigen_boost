#ifndef _MYSTRING
#define _MYSTRING

class mystring { 
  const char* buffer; 
  int length;
  public: 
    mystring() : buffer(nullptr), length(0) {}
    void setbuffer(const char* s) { buffer = s; length = strlen(s); } 
    const char& operator[ ] (const int index) { return buffer[index]; }
    int size( ) { return length; }
 }; 

#endif
