# Overview

3 C++ projects are included herein. Their makefiles have been changed to support [hcc](https://bitbucket.org/multicoreware/hcc).

- [LCALS](https://codesign.llnl.gov/LCALS.php)
- [Eigen Test Suite](http://eigen.tuxfamily.org)
- [Boost C++ Test Framework](http://www.boost.org/doc/libs/1_60_0/libs/test/doc/html/index.html)

---

# Test Configuration
LCALS and Eigen were tested with the following 2 compilers:

- g++
```
g++ (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4
Copyright (C) 2013 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

- hcc
```
HCC clang version 3.5.0  (based on HCC 0.10.16045-9093b9e-efd6099-5a1009a LLVM 3.5.0svn)
Target: x86_64-unknown-linux-gnu
Thread model: posix

```

---

# LCALS
LCALS version 1.0.2 is hosted on this repository. 

## Test Results
Test results suggest LCALS works fine on both g++ and hcc.

- [LCALS g++ test log](https://bitbucket.org/whchung/hcc_lcals_eigen_boost/src/516a78291d87c00675125bb773bd355169cecea9/test_log/lcals_g++.log?at=master&fileviewer=file-view-default)
- [LCALS hcc test log](https://bitbucket.org/whchung/hcc_lcals_eigen_boost/src/516a78291d87c00675125bb773bd355169cecea9/test_log/lcals_hcc.log?at=master&fileviewer=file-view-default)

## Build LCALS
To build LCALS, go into ```lcals_v1_0_2``` directory, and invoke ```make```. By default it would try build the project using hcc compiler installed under ```/opt/hcc``` directory. It could be changed by changing Makefile in the directory.

To build LCALS with g++, please comment out this line:

```LCALS_ARCH = hcc```

and uncomment the line:

```#LCALS_ARCH = x86_sse_gnu```

## Execute LCALS
To execute it, simply run ```lcals.exe``` after building is complete.

---

# Eigen

Eigen version 3.2.7, git commit #b30b872 is hosted on this repository.

## Eigen Test Results
Test results of it with g++ and hcc are as follows:

- g++ [Eigen g++ test log](https://bitbucket.org/whchung/hcc_lcals_eigen_boost/src/516a78291d87c00675125bb773bd355169cecea9/test_log/eigen_g++.log?at=master&fileviewer=file-view-default#eigen_g++.log-1930)
    - 2 tests failed out of 642
        - gmres_2 (Failed)
        - levenberg_marquardt (Failed)
- hcc [Eigen hcc test log](https://bitbucket.org/whchung/hcc_lcals_eigen_boost/src/516a78291d87c00675125bb773bd355169cecea9/test_log/eigen_hcc.log?at=master&fileviewer=file-view-default#eigen_hcc.log-1864)
    - 3 test sets were excluded because they couldn't be built by hcc
        - geo_quaternion
        - matrix_exponential
        - matrix_function
    - 3 tests failed out of 620
        - commainitializer (Failed)
        - product_trmv_2 (Failed)
        - levenberg_marquardt (Failed)

## Build and Execute Eigen with g++

To build Eigen with g++, go into ```eigen-eigen-b30b87236a1b``` directory, and invoke:

```
mkdir build_g++
cd build_g++
cmake ..
make check
```

Eigen would then be built and all tests be executed after build is complete.

## Build and Execute Eigen with hcc

To build Eigen with hcc, go into ```eigen-eigen-b30b87236a1b``` directory, and invoke:

```
mkdir build_hcc
cd build_hcc
cmake -DBUILD_WITH_HCC ..
make check
```

Eigen would then be built and all tests be executed after build is complete.

---

# Boost.Test

Boost version 1.60.0 is hosted on this repository. A simple example was devised to show Boost.Test could be used with hcc.

```
cd boost_1_60_0/hcc_test
make
./boost_test
```

